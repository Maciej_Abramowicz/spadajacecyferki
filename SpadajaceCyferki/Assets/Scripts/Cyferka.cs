﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Cyferka : MonoBehaviour
{
    [SerializeField] Sprite spriteSplash;
    [SerializeField] Sprite[] spritesCyferek;
    
    RectTransform rt;
    SoundManager soundM;
    float predkoscOpadania = 100f;
    GameManager gm;
    Parzystosc parz;
    Animator anim;
    int wybranaLiczba;
    void Start()
    {
        soundM = GameObject.FindGameObjectWithTag("SoundManager").GetComponent<SoundManager>();
        anim = GetComponent<Animator>();
        gm = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        parz = GameObject.FindGameObjectWithTag("GameManager").GetComponent<Parzystosc>();
        
        PrzypiszWartoscCyferki();
        rt = GetComponent<RectTransform>();
    }
    void PrzypiszWartoscCyferki()
    {
        int losowaLiczba = Random.Range(1, 7);
        wybranaLiczba = losowaLiczba;
        GetComponent<Image>().sprite = spritesCyferek[losowaLiczba - 1];
        
    }

    public void AkcjaPoKliknieciu()
    {
        gm.DodajPunkty(wybranaLiczba);
        parz.PobierzKliknietaCyferke(wybranaLiczba);
        AktywujSplasha();
        anim.SetTrigger("Znikanie");
        ZmienKolorPrzycisku();
        GetComponent<Button>().interactable = false;
        soundM.OdtworzDzwiek(0);
        UsunCyferke(0.5f);
    }
    void ZmienKolorPrzycisku()
    {
        ColorBlock blokKolorow = GetComponent<Button>().colors;
        switch (wybranaLiczba)
        {
            case 1: 
                blokKolorow.disabledColor = Color.red;
                break;
            case 2: 
                blokKolorow.disabledColor = Color.yellow;
                break;
            case 3: 
                blokKolorow.disabledColor = Color.green;
                break;
            case 4: 
                blokKolorow.disabledColor = Color.blue;
                break;
            case 5: 
                blokKolorow.disabledColor = new Color(0.992f, 0.435f, 0f, 1f);
                break;
            case 6:
                blokKolorow.disabledColor = new Color(1f, 0f, 0.972f, 1f);
                break;
            default:
                blokKolorow.disabledColor = Color.cyan;
                break;

        }
        GetComponent<Button>().colors = blokKolorow;
    }
    void AktywujSplasha()
    {
        GetComponent<Image>().sprite = spriteSplash;
    }
    void SpadajWDol()
    {
        rt.anchoredPosition -= new Vector2(0f, 1f) * predkoscOpadania * Time.deltaTime;
    }
    // Update is called once per frame
    void Update()
    {
        SpadajWDol();
        if(rt.anchoredPosition.y < -400)
        {
            UsunCyferke(0f);
        }
    }
    void UsunCyferke(float time)
    {
        Destroy(gameObject, time);
    }
}
